#include <iostream>
#include <stdlib.h>

using namespace std;

void NaborTochek(double* x, double* y, int num) {
    srand(time(NULL));
    for(int i = 0; i < num; i++) {
        x[i] = rand()%20;
        x[i] = x[i]/10-1;
        y[i] = rand()%10;
        y[i] = y[i]/10;
    }
}

bool Popadanie(double x, double y) {
   if(x <= 0 && x >= -1) {
       if(x >= -0.5 && y >= 0.5) {
           if(x+y >= 0 && x + y <= 0.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x <= -0.5 && y >= 0.5) {
           if(x-y <= -1 && x-y >= -1.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x >= -0.5 && y <= 0.5) {
           if(x-y >= -1 && x-y <= -0.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x <= -0.5 && y <= 0.5) {
           if(x+y <= 0 && x+y >= -0.5) {
               return true;
           } else {
               return false;
           }
       }
       
   }
   
   if(x >= 0 && x <= 1) {
      if(x >= 0.5 && y >= 0.5) {
           if(x+y >= 1 && x + y <= 1.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x <= 0.5 && y >= 0.5) {
           if(x-y <= 0 && x-y >= -0.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x >= 0.5 && y <= 0.5) {
           if(x-y >= 0 && x-y <= 0.5) {
               return true;
           } else {
               return false;
           }
       }
       if(x <= 0.5 && y <= 0.5) {
           if(x+y >= 0.5 && x+y <= 1) {
               return true;
           } else {
               return false;
           }
       } 
   }
   return false;
}

int main()
{  
    double count = 0.0;
    int num;
    cout << "" << endl;
    cin >> num;
    double* x_cord = new double[num];
    double* y_cord = new double[num];
    NaborTochek(x_cord,y_cord,num);
    for(int i = 0; i < num; i++) {
        if(Popadanie(x_cord[i],y_cord[i])) {
           cout << "Выстрел с координатой (" << x_cord[i] << ";" << y_cord[i] << ") попал в мишень" << endl; 
           count++;
        } else {
            cout << "Выстрел с координатой (" << x_cord[i] << ";" << y_cord[i] << ") не попал в мишень" << endl; 
        }
    }
    cout << (count/num)*100 << "%" << endl;
}